
/**
 * 组件混入封装
 * 上传
 */
export default {
	methods: {
		
		// 上传图片
		uploadImg(url){
			let that = this
			uni.chooseImage({
			    count: 1, //默认9
			    sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
			    sourceType: ['album'], //从相册选择
			    success: function (res) {
					// console.log(res)
					
					that.uploadFile({
						url:that.config.interfaceUrl + url,
						src: res.tempFilePaths[0],
						name:'image',
					}).then(r=>{
						r = JSON.parse(r.data)
						// console.log(r)
						if(r.code == 1){
							typeof that.getDataInfo == 'function' ? that.getDataInfo() :''
							if(that.editorCtx){
								that.editorCtx.insertImage({
									src: that.config.mediaUrl + r.data.url, // 此处需要将图片地址切换成服务器返回的真实图片地址
									alt: '图片',
									success: function(e) {}
								});
							}
							uni.showToast({title:r.msg,icon:'none'})
						}else{
							uni.showToast({title:r.msg,icon:'none'})
						}
					})
					
			    }
			});
		},
		
		uploadFile(params) {
			const that = this
			if (!params.hideLoading) {
				uni.showLoading({
					mask: true,
					title: '请稍候...'
				})
			}
			return new Promise((resolve, reject) => {
				const uploadTask = uni.uploadFile({
					url: params.url,
					filePath: params.src,
					name: params.name||'file',
					// files: params.files||'',
					header: {
						// 'content-type': 'multipart/form-data',
						'token': uni.getStorageSync('token')
					},
					formData: params.formData||{},
					success: function(res) {
						// console.log(res);
						uni.hideLoading()
						resolve(res);
					},
					fail: function(res) {
						reject(res)
						uni.hideLoading();
					}
				})
			})
		},
		
		
	},
}
