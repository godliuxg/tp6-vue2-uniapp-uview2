
/*
*  获取手机授权
*/

import {getMemberPhone} from '@/service/api.js'

export default {
	methods:{
		
		
		// 获取手机号码
		get_phone(e){
			console.log(e);
			return
			
			let that = this;
			if(e.detail.errMsg!='getPhoneNumber:ok'){
				console.log('用户取消授权');
				return false;
			}
	
			let data = {
				encryptedData:e.detail.encryptedData,
				iv:e.detail.iv,
			}
					
			getMemberPhone(data).then(r1=>{
				console.log(r1);
				if(r1.code){
					that.show('微信登录成功')
					that.toIndex()
				}
			})
			
		},
		
		
		
	}
}