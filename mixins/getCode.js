

import {getPhoneCode} from '@/service/api.js'

import common from '@/mixins/common.js'

export default {
	mixins:[common],
	data(){
		return {
			
		}
	},
	methods:{
		//获取短信验证码
		getCode() {
			var that = this;
			if (!that.phone) {
				that.showToast('请输入手机号');
				return;
			}
			if (!/^[1][3,4,5,6,,7,8,9][0-9]{9}$/.test(that.phone)) {
				that.showToast('请输入正确手机号');
				return;
			}
			
			let data = {
				telephone:that.phone,
			}
			console.log(getPhoneCode)
			//这里请求后台获取短信验证码
			getPhoneCode(data).then(res=>{
				if(res.code == 1){
					this.showToast(res.msg);
					// 倒计时
					var interval = setInterval(() => {
						that.showText = false;
						var times = that.second - 1;
						//that.second = times<10?'0'+times:times ;//小于10秒补 0
						that.second = times;
						// console.log(times);
					}, 1000);
					setTimeout(() => {
						clearInterval(interval);
						that.second = 60;
						that.showText = true;
					}, 60000);
					
				}else{
					that.showToast(res.msg );
				}
			})
		},
	}
}