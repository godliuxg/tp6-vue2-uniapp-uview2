

// 微信支付
export default {
	methods:{
		wxPay(data) {
		 return new Promise((resolve, reject) => {
			  uni.requestPayment({
				  provider: 'wxpay',
				  timeStamp: data.timeStamp,
				  nonceStr: data.nonceStr,
				  package: data.package,
				  signType: data.signType,
				  paySign: data.paySign,
				  success: function (res) {
					   resolve(res)
				  },
				  fail: function (err) {
					   reject(err)
				  }
			  });
			})
		}
	}
}