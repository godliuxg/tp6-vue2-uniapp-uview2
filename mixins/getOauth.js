
/*
* uniapp api 获取授权方法
*/


export default {
	data(){
		UserInfo:''
		iv:''
		encryptedData:''
	},
	onLoad(){
		
	},
	methods:{
		// 获取用户信息。每次请求都会弹出授权窗口，用户同意后返回 userInfo
		getUserProfile(){
			uni.getUserProfile({
				desc:'测试',
				success:(res)=>{
					console.log(res)
					this.userInfo = res.userInfo
					this.iv = res.iv
					this.encryptedData = res.encryptedData
				},
				fail: (err) => {
					console.log(err)
				},
				complete(all) {
					// console.log(all)
				}
			})
		},
		
		// 调起客户端小程序设置界面，返回用户设置的操作结果。
		openSetting(){
			uni.openSetting({
			  success(res) {
			    console.log(res.authSetting)
			  }
			});
		},
		
		// 获取用户的当前设置
		getSetting(){
			uni.getSetting({
			  success(res) {
			    console.log(res.authSetting)
			  }
			});
		},
		
		// 获取用户收货地址。调起用户编辑收货地址原生界面，并在编辑完成后返回用户选择的地址，需要用户授权 scope.address。
		chooseAddress(){
			uni.chooseAddress({
			  success(res) {
			    console.log(res.userName)
			    console.log(res.postalCode)
			    console.log(res.provinceName)
			    console.log(res.cityName)
			    console.log(res.countyName)
			    console.log(res.detailInfo)
			    console.log(res.nationalCode)
			    console.log(res.telNumber)
			  }
			})
		},
		
		// 提前向用户发起授权请求
		getOauthOption(scope){
			uni.authorize({
			    scope: scope,
			    success() {
			        uni.getLocation()
			    }
			})
		}
		
	}
}