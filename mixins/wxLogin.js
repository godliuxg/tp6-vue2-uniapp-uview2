/*
 *  微信登录
 */

import {
	getMemberOpenId
} from '@/service/api.js'

import {  
        mapMutations  
    } from 'vuex';
import common from '@/mixins/common.js'
export default {
	mixins:[common],
	methods: {
		// 引入vuex登录方法
		...mapMutations(['saveUser']),
		// 微信登录 获取用户信息 包括头像 昵称
		wxLogin() {
			let that = this
			return new Promise((resolve, reject) => {
				uni.getUserProfile({
					desc: '获取用户个人信息',
					success: (r3) => {
						this.userInfo = r3.userInfo

						uni.login({
							success(r1) {
								let data = {
									code: r1.code,
									user_info: r3.userInfo,
									share_uid: uni.getStorageSync('share_uid') || 0
								}
								getMemberOpenId(data).then(r2 => {
									if (r2.code == 200) {
										// that.showToast(r2.msg)
										uni.setStorageSync('token', r2.data.member_token)
										// uni.setStorageSync('userInfo', r3.userInfo)
										// uni.setStorageSync('hasLogin', true)
										// 改为使用vuex状态修改方法
										that.saveUser(r3.userInfo)
										resolve(r2)
									}

								})
							}
						})
					},
					fail: (err) => {
						that.showToast('授权失败', 'none')
						console.log(err)
					}
				})
			})
		},
	}
}
