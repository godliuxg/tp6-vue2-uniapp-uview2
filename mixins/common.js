export default {
	data(){
		return {
			
		}
	},
	onLoad(option) {
		
	},
	
	methods:{
		// 下拉刷新 混入 无作用
		onPullDownRefresh() {
			console.log('refresh');
		},
		
		// 提示
		showToast(title,icon='none'){
			uni.showToast({
				title,
				icon
			})
		},
		
		// 图片处理
		imgUrl(url){
			if(url.includes('upload')){
				url = this.config.mediaUrl + url
			}
			return url
		},
		
		// 获取当前页面路径及参数
		getCurrentPageInfo(){
			let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
			let curRoute = routes[routes.length - 1].route //获取当前页面路由
			let curParam = routes[routes.length - 1].options; //获取路由参数
			let params = ''
			for (let key in curParam) {
				params += '&' + key + '=' +curParam[key]
			}
			params = params.substr(1)
			
			return `${curRoute}?${params}`
		},
		
		// 预览图片多张
		previewImg(imgs) {
			if(!imgs){
				return '';
			}
			
			let _this = this;
			let imgsArray = [];
			
			Array.isArray(imgs) ? imgsArray = imgs : imgsArray.push(imgs)
			console.log(imgsArray)
			
			uni.previewImage({
				current: 0,
				urls: imgsArray,
				indicator: 'number',
				loop: true
			});
		
		},
		
	}
}