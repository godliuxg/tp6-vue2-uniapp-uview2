import request from './request.js'
import apiConfig from './apiConfig.js'

let api = {}

for (let item in apiConfig) {
    api[item] = (data) => request({url: apiConfig[item] ,data })
}


module.exports = api