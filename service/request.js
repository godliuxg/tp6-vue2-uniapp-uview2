/**
 * 通用uni-app网络请求
 * 基于 Promise 对象 request 使用方式
 */

import config from './config.js'
import {getSceneInfo,getFissionInfo} from './apiConfig.js'

import Vue from 'vue'
Vue.prototype.config = config

const baseUrl = config.interfaceUrl


export default (params)=>{
	// 接口请求
	let that = this;
	let token = uni.getStorageSync('token') || ''
	
	
	// let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
	// let curRoute = routes[routes.length - 1].route //获取当前页面路由
	// let curParam = routes[routes.length - 1].options; //获取路由参数
	// // 拼接参数
	// let param = ''
	// for (let key in curParam) {
	// 	param += '&' + key + '=' + curParam[key]
	// }
	// let data = {}
	// data.curRoute = curRoute
	// data.curParam = curParam
	
	// console.log(111,data)
	// console.log(token)
	// if(!token ){
	// 	if(params.url == getFissionInfo || params.url == getSceneInfo){
			
	// 	}else{
	// 		uni.reLaunch({
	// 			url:'/pages/login/login'
	// 		})
	// 		console.log('没有token')
	// 		return false
	// 	}
		
	// }
	
	if (!params.hideLoading) {
		uni.showLoading({
			mask: true,
			title: '请稍候...'
		})
	}
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseUrl +  params.url,
			data: params.data?params.data:'',
			header: {
				'content-type': params.type ? 'application/x-www-form-urlencoded' : 'application/json',
				'MemberToken': token
			},
			method: params.method ? params.method:'POST', //'GET','POST'
			dataType: 'json',
			success: (res) => {
				!params.hideLoading && uni.hideLoading()
				
				if(res.data.code == 300){
					// uni.showToast({
					// 	title:res.data.msg,
					// 	icon:'none'
					// })
					reject(res.data.code)
					// setTimeout(()=>{
					// 	uni.navigateTo({
					// 		url:'/pages/login/login'
					// 	})
					// },800)
					return false
				}
				
				resolve(res.data)
			},
			fail: (res) => {
				if (!params.hideLoading) {
					uni.showToast({ title: '网络不给力，请稍后再试~' , icon:'none'});
				}
				reject(res)
			}
		})
	})
}