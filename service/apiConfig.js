/*
	api 接口配置
*/

module.exports = {
	getMemberOpenId: "/Login/mini", // 用户登陆，获取openid与sessionkey
	getMemberPhone: "/Member/bindPhoneMini", // 解密用户手机号
	getMemberProfile: "/Member/info", // 获取用户详细信息
	setMemberProfile: "/Member/edit", // 设置用户详细信息
}
